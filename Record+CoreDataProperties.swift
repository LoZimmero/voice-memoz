//
//  Record+CoreDataProperties.swift
//  Voice Memoz
//
//  Created by Gabriele Fioretti on 14/12/2019.
//  Copyright © 2019 Gabriele Fioretti. All rights reserved.
//
//

import Foundation
import CoreData


extension Record {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Record> {
        return NSFetchRequest<Record>(entityName: "Record")
    }

    @NSManaged public var audio: Data?
    @NSManaged public var date: Date?
    @NSManaged public var lenght: Double
    @NSManaged public var title: String?
    @NSManaged public var location: String?

}
