//
//  CustomTableViewCell.swift
//  Voice Memoz
//
//  Created by Gabriele Fioretti on 11/12/2019.
//  Copyright © 2019 Gabriele Fioretti. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import AVFoundation

class CustomTableViewCell: UITableViewCell {
    
    //MARK: - Variables
    var avPlayer: AVAudioPlayer!
    var isPlaying: Bool = false
    var recordingSession: AVAudioSession!
    var timer: Timer?
    var indexOfDatabase: Int?
    var indexPath: IndexPath!
    
    //MARK: - Outlets
    @IBOutlet weak var titleLabel: UITextField!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var lenghtLabel: UILabel!
    @IBOutlet weak var optionButton: UIButton!
    @IBOutlet weak var back15Button: UIButton!
    @IBOutlet weak var next15Button: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var progressionSlider: UISlider!
    @IBOutlet weak var timeElapsedLabel: UILabel!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        if (editing) {
            self.lenghtLabel.isHidden = true
        } else {
            self.lenghtLabel.isHidden = false
        }
    }
    
    //MARK: - Play button function
    @IBAction func play(_ sender: UIButton) {
        
        if (!isPlaying){
            do{
                recordingSession = AVAudioSession.sharedInstance()
                try recordingSession.setCategory(.playAndRecord, mode: .default)
                try recordingSession.setActive(true)
                avPlayer.delegate = self
                progressionSlider.value = 0.0
                progressionSlider.maximumValue = Float(avPlayer.duration)
                avPlayer.prepareToPlay()
                avPlayer.play()
                playButton.setBackgroundImage(UIImage(systemName: "pause.fill"), for: .normal)
                isPlaying = true
                timer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
            } catch let error as NSError{
                print("Error in converting audio to AVPlayer.\n\(error.userInfo)")
            }
        } else {
            avPlayer.pause()
            playButton.setBackgroundImage(UIImage(systemName: "play.fill"), for: .normal)
            isPlaying = false
        }
    }
    
    //MARK: - Slider function
    @IBAction func sliderScroll(_ sender: UISlider) {
        avPlayer?.currentTime = Float64(progressionSlider.value)
    }
    
    //MARK: - Edit title
    @IBAction func editText(_ sender: UITextField) {
        if (sender.text == ""){
            return
        }
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<Record>(entityName: "Record")
        do {
            let records = try managedContext.fetch(fetchRequest)
            records[indexOfDatabase!].setValue(sender.text, forKey: "title")
            try managedContext.save()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    //MARK: - Function to delete the cell
//    @IBAction func deleteCell(_ sender: UIButton) {
////        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
////        let managedContext = appDelegate.persistentContainer.viewContext
////        let fetchRequest = NSFetchRequest<Record>(entityName: "Record")
////        let rootVc = self.window?.rootViewController as! ListViewController
////        do {
////            var records = try managedContext.fetch(fetchRequest)
////            managedContext.delete(records[indexOfDatabase!])
////            rootVc.tableView.beginUpdates()
////            records.remove(at: indexPath.row)
////            rootVc.tableView.deleteRows(at: [indexPath], with: .fade)
////            rootVc.tableView.endUpdates()
////            try managedContext.save()
////            rootVc.tableView.reloadData()
////        } catch let error as NSError {
////            print("Could not fetch. \(error), \(error.userInfo)")
////        }
    //    }
    
    //MARK: - Funcion fot next/back 15 buttons
    @IBAction func back15(_ sender: UIButton) {
        guard let player = avPlayer else { return }
        player.currentTime -= 15
        progressionSlider.value = Float(player.currentTime)
    }
    
    @IBAction func next15(_ sender: UIButton) {
        guard let player = avPlayer else { return }
        player.currentTime += 15
        progressionSlider.value = Float(player.currentTime)
        //Update labels
        let remainingTimeInSeconds = avPlayer.duration - avPlayer.currentTime
        timeRemainingLabel.text = getFormattedTime(timeInterval: remainingTimeInSeconds)
        timeElapsedLabel.text = getFormattedTime(timeInterval: player.currentTime)
    }
    
    //MARK: - Timer function to update from/to slider
    @objc func updateSlider(){
        guard let player = avPlayer else { return }
        progressionSlider.value = Float(player.currentTime)
        // Update time remaining label
        let remainingTimeInSeconds = avPlayer.duration - avPlayer.currentTime
        timeRemainingLabel.text = getFormattedTime(timeInterval: remainingTimeInSeconds)
        timeElapsedLabel.text = getFormattedTime(timeInterval: player.currentTime)
    }
    
    //MARK: - Function to implement share
    @IBAction func optionAction(_ sender: UIButton) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Record>(entityName: "Record")
        do{
            let records = try managedContext.fetch(fetchRequest)
            let record = records[indexOfDatabase!]
            let firstActivityItem = dataToFile(fileName: ("\(record.value(forKey: "title") as! String).m4a" ), record: record)
            let activityViewController : UIActivityViewController = UIActivityViewController(
                activityItems: [firstActivityItem!], applicationActivities: nil)
            self.window?.rootViewController!.present(activityViewController, animated: true)
        } catch let error as NSError{
            print(error.userInfo)
        }
    }
    
    //MARK: - Function to format the time
    func getFormattedTime(timeInterval: TimeInterval) -> String {
        let mins = timeInterval / 60
        let secs = timeInterval.truncatingRemainder(dividingBy: 60)
        let timeformatter = NumberFormatter()
        timeformatter.minimumIntegerDigits = 2
        timeformatter.minimumFractionDigits = 0
        timeformatter.roundingMode = .down
        guard let minsStr = timeformatter.string(from: NSNumber(value: mins)), let secsStr = timeformatter.string(from: NSNumber(value: secs)) else {
            return ""
        }
        return "\(minsStr):\(secsStr)"
    }
    
    
    //MARK: - Functions to show/hide cell elements
    func disableCellElements() {
        self.playButton.isHidden = true
        self.playButton.isEnabled = false
        self.next15Button.isHidden = true
        self.next15Button.isEnabled = false
        self.back15Button.isHidden = true
        self.back15Button.isEnabled = false
        self.deleteButton.isHidden = true
        self.deleteButton.isEnabled = false
        self.optionButton.isHidden = true
        self.optionButton.isEnabled = false
        self.progressionSlider.isHidden = true
        self.progressionSlider.isEnabled = false
        self.timeElapsedLabel.isHidden = true
        self.timeRemainingLabel.isHidden = true
        self.titleLabel.isEnabled = false
    }
    
    func enableCellElements() {
        self.playButton.isHidden = false
        self.playButton.isEnabled = true
        self.next15Button.isHidden = false
        self.next15Button.isEnabled = true
        self.back15Button.isHidden = false
        self.back15Button.isEnabled = true
        self.deleteButton.isHidden = false
        self.deleteButton.isEnabled = true
        self.optionButton.isHidden = false
        self.optionButton.isEnabled = true
        self.progressionSlider.isHidden = false
        self.progressionSlider.isEnabled = true
        self.timeElapsedLabel.isHidden = false
        self.timeRemainingLabel.isHidden = false
        self.titleLabel.isEnabled = true
    }
    
    //MARK: - To get data from cordata as URL
    /// - Returns: the Current directory in NSURL
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    //MARK: Data into file
    ///
    /// - Parameters:
    ///   - fileName: the Name of the file you want to write
    ///   - record: The record fetched from coredata
    /// - Returns: Returns the URL where the new file is located in NSURL
    func dataToFile(fileName: String, record: NSManagedObject) -> NSURL? {
        do {
            // Make a constant from the data
            let data = record.value(forKey: "audio") as! Data
            // Make the file path (with the filename) where the file will be loacated after it is created
            let filePath = getDocumentsDirectory().appendingPathComponent(fileName)
            // Write the file from data into the filepath (if there will be an error, the code jumps to the catch block below)
            try data.write(to: URL(fileURLWithPath: filePath))
            
            // Returns the URL where the new file is located in NSURL
            return NSURL(fileURLWithPath: filePath)
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            // Returns nil if there was an error in the do-catch -block
            return nil
        }
        
    }
    
}

//MARK: - Extension AVAudioPlayerDelegate
extension CustomTableViewCell: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        avPlayer.stop()
        timer?.invalidate()
        isPlaying = false
        playButton.setBackgroundImage(UIImage(systemName: "play.fill"), for: .normal)
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }
}
