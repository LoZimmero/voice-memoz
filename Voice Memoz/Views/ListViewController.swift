//
//  ViewController.swift
//  Voice Memoz
//
//  Created by Gabriele Fioretti on 06/12/2019.
//  Copyright © 2019 Gabriele Fioretti. All rights reserved.

import UIKit
import CoreData
import AVFoundation
import CoreLocation
import Foundation

class ListViewController: UIViewController {
    //MARK: - Variables
    var records: [NSManagedObject] = []
    var avRecorder: AVAudioRecorder!
    var avSession: AVAudioSession!
    var avPlayer: AVAudioPlayer!
    var buttonCenter: CGPoint = CGPoint()
    var date: Date?
    var filteredTableData = [NSManagedObject]()
    var resultSearchController = UISearchController()
    var selectedRowIndex: NSIndexPath = NSIndexPath(row: -10, section: 0)
    let locationManager = CLLocationManager()
    var locality: String!
    var editButton: UIBarButtonItem!
    var selectedRecordsIndexes: [Int] = []
    
    let recordSettings = [
        AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
        AVSampleRateKey: 12000,
        AVNumberOfChannelsKey: 1,
        AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
    ]
    
    //MARK: - Outlets
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var recordButtonView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    
    
    //MARK: - Function to retrieve database path
    func whereIsMySQLite() {
        let path = FileManager
            .default
            .urls(for: .applicationSupportDirectory, in: .userDomainMask)
            .last?
            .absoluteString
            .replacingOccurrences(of: "file://", with: "")
            .removingPercentEncoding
        
        print(path ?? "Not found")
    }
    
    
    
    //MARK: - Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeTableView()
        initializeViewElements()
        
        //I request permission here to not override mic permission ask
        self.locationManager.requestWhenInUseAuthorization()
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<Record>(entityName: "Record")
        do {
            records = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.sizeToFit()
            tableView.tableHeaderView = controller.searchBar
            return controller
        })()
        
        // Reload the table
        tableView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        avRecorder?.stop()
    }
    
    //MARK: - Functions
    @IBAction func buttonAction(_ sender: UIButton) {
        
        do {
            avSession = AVAudioSession.sharedInstance()
            try avSession.setCategory(AVAudioSession.Category.playAndRecord)
            try avSession.setActive(true)
            
            initializeLocationManager()
            
            avSession.requestRecordPermission() { allowed in DispatchQueue.main.async {
                if !allowed {
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                print("Settings opened: \(success)")
                            })
                        }
                    }
                    
                    let permitAlert = UIAlertController(title: "Alert", message: "If you want to record, you have to enable your mic first", preferredStyle: UIAlertController.Style.alert)
                    permitAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel) {action in
                        return
                    })
                    permitAlert.addAction(settingsAction)
                    self.present(permitAlert, animated: true, completion: nil)
                } else {
                    if self.avRecorder == nil {
                        self.startRecording()
                        self.recordingAnimations()
                        self.date = Date()
                    } else {
                        self.stopAnimations()
                        self.finishRecording(success: true)
                    }
                }
                }
            }
        } catch {
            // failed to record!
            print("Failed to record")
            finishRecording(success: false)
        }
    }
    
    //MARK: - Delete button action
    @IBAction func deleteAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to delete this recording?", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Yes", style: .destructive) { (action) in
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            let managedContext = appDelegate.persistentContainer.viewContext
            DispatchQueue.main.async {
                do{
                    self.tableView.beginUpdates()
                    for index in self.selectedRecordsIndexes {
                        let commit = self.records[index]
                        managedContext.delete(commit)
                        try managedContext.save()
                        self.tableView.reloadData()
                    }
                    self.tableView.endUpdates()
                } catch let error as NSError{
                    print("CIAO, \(error.userInfo)")
                }
            }
            
            self.selectedRecordsIndexes.removeAll()
        }
        self.isEditing = false
        editButton.title = "Edit"
        editButton.style = .plain
        self.deleteButton.isHidden = true
        self.deleteButton.isEnabled = false
        for iP in tableView.indexPathsForRows(in: self.view.frame)! {
            tableView.cellForRow(at: iP)?.setEditing(false, animated: true)
            tableView.cellForRow(at: iP)?.setSelected(false, animated: true)
        }
        let cancelAction = UIAlertAction(title: "No", style: .cancel)
        alert.addAction(confirmAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
    //MARK: - Initialization functions
    func initializeLocationManager() {
        // For use in foreground
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                locality = ""
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            @unknown default:
                break
            }
        } else {
            return
        }
    }
    
    
    func initializeTableView() {
        self.navigationController!.navigationBar.sizeToFit()
//        editButton = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editMode))
//        self.navigationItem.rightBarButtonItem = editButton
        self.tableView.allowsMultipleSelection = true
        self.tableView.allowsMultipleSelectionDuringEditing = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "CustomTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomTableViewCell")
    }
    
    //MARK: - Function for the edit button
    @objc func editMode() {
        if (self.isEditing) {
            self.isEditing = false
            editButton.title = "Edit"
            editButton.style = .plain
            self.deleteButton.isHidden = true
            self.deleteButton.isEnabled = false
            for iP in tableView.indexPathsForRows(in: self.view.frame)! {
                tableView.cellForRow(at: iP)?.setEditing(false, animated: true)
                tableView.cellForRow(at: iP)?.setSelected(false, animated: true)
            }
        } else {
            self.isEditing = true
            editButton.title = "Done"
            editButton.style = .done
            self.deleteButton.isHidden = false
            self.deleteButton.isEnabled = true
            let indexPath = selectedRowIndex
            selectedRowIndex = NSIndexPath(row: -1, section: 0)
            tableView.reloadRows(at: [(indexPath as IndexPath)], with: .fade)
            for iP in tableView.indexPathsForRows(in: self.view.frame)!{
                tableView.cellForRow(at: iP)?.setEditing(true, animated: true)
            }
        }
    }
    
    func initializeViewElements() {
        self.view.backgroundColor = UIColor(named: "BackgroundColor")
        recordButton.layer.cornerRadius = 0.5 * recordButton.bounds.size.width
        recordButton.clipsToBounds = true
        buttonCenter = recordButton.center
        buttonView.layer.cornerRadius = 5
        recordButtonView.layer.cornerRadius = 0.5 * recordButtonView.bounds.size.width
        recordButtonView.layer.borderWidth = 5
        recordButtonView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        recordButtonView.backgroundColor = #colorLiteral(red: 0.1567457616, green: 0.1567792594, blue: 0.1567413509, alpha: 1)
        recordButtonView.center = recordButton.center
    }
    
    //MARK: - Animations
    func recordingAnimations(){
        UIButton.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: {
            self.recordButton.layer.cornerRadius = 5
            self.recordButton.layer.frame.size = CGSize(width: 30, height: 30)
            self.recordButton.center = self.buttonCenter
        }, completion: nil)
    }
    
    func stopAnimations() {
        UIButton.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: {
            self.recordButton.layer.frame.size = CGSize(width: 70, height: 70)
            self.recordButton.layer.cornerRadius = 0.5 * self.recordButton.bounds.size.width
            self.recordButton.center = self.buttonCenter
        }, completion: nil)
    }
    
    //MARK: - Funtions for recording
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        do {
            avRecorder = try AVAudioRecorder(url: audioFilename, settings: recordSettings)
            avRecorder.delegate = self as? AVAudioRecorderDelegate
            avRecorder.record()
        } catch {
            finishRecording(success: false)
        }
    }
    
    func finishRecording(success: Bool) {
        locationManager.stopUpdatingLocation()
        let lenght = avRecorder.currentTime
        avRecorder.stop()
        
        if !success {
            let errRecordAlert = UIAlertController(title: "Recording error", message: "Error during recording", preferredStyle: UIAlertController.Style.alert)
            errRecordAlert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(errRecordAlert, animated: true, completion: nil)
            return
        }
        
        var audioTrack: Data?
        do {
            audioTrack = try Data(contentsOf: avRecorder.url)
        } catch let error as NSError {
            print("\(error)\n\(error.description)")
        }
        
        let title: String
        
        var count = 0
        for record in records{
            if (record.value(forKey: "location") as? String == locality){
                count += 1
            }
        }
        
        title = (locality == "" ? "Recording \(count)" : "\(locality ?? "Recording") \(count)")
        
        save(title: title, audio: audioTrack!, date: date, lenght: lenght, location: locality)
        tableView.reloadData()
        avRecorder = nil
        
    }
    
    //MARK: - Func to store in CoreData
    func save(title: String?, audio: Data?, date: Date?, lenght: Double?, location: String?) -> Void{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Record", in: managedContext)!
        let record = NSManagedObject(entity: entity, insertInto: managedContext)
        
        record.setValue(title, forKey: "title")
        record.setValue(audio, forKey: "audio")
        record.setValue(lenght, forKey: "lenght")
        record.setValue(date, forKey: "date")
        record.setValue(location, forKey: "location")
        
        do{
            records.append(record)
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save.\n\(error), \(error.userInfo)")
        }
    }
}
//MARK: - EXTENSIONS

//MARK:Extension for tableView
extension ListViewController: UITableViewDataSource, UITableViewDelegate, AVAudioPlayerDelegate, UISearchResultsUpdating{
    
    //MARK: - Creation of the cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let record: NSManagedObject
        
        if (resultSearchController.isActive) {
            record = filteredTableData[indexPath.row]
        }
        else {
            record = records[indexPath.row]
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as! CustomTableViewCell
        
        cell.titleLabel.text = (record.value(forKey: "title") as? String) ?? "SOLE"
        let dateFormatter = DateFormatter()
        let date = record.value(forKey: "date") as? Date ?? Date()
        let descriptionString: String
        if date.distance(to: Date()) < 86400 {
            dateFormatter.dateFormat = "hh:mm"
            descriptionString = "Today \(dateFormatter.string(from: date))"
        } else if (date.distance(to: Date()) < 172800) {
            dateFormatter.dateFormat = "hh:mm"
            descriptionString = "Yesterday \(dateFormatter.string(from: date))"
        } else {
            dateFormatter.dateFormat = "dd/MM/yyy hh:mm"
            descriptionString = dateFormatter.string(from: date)
        }
        cell.descriptionLabel.text = descriptionString
        let lenght = record.value(forKey: "lenght") as? Double ?? 0.0
        
        //MARK: Suoper closure to convert double in time
        let timeLenght = { (lenght) -> String in
            let hours: Int = Int(lenght / 3600)
            let minutes: Int = Int(lenght.remainder(dividingBy: 3600) / 60)
            let seconds: Int = Int(lenght.remainder(dividingBy: 60))
            
            if (hours == 0){
                return NSString(format: "%02d : %02d", minutes, seconds) as String
            }
            return NSString(format: "%02d : %02d : %02d", hours, minutes, seconds) as String
        }(lenght)
        cell.lenghtLabel.text = timeLenght
        
        do{
            avPlayer = try AVAudioPlayer(data: record.value(forKey: "audio") as! Data)
        } catch let error as NSError {
            print("Error in retrieving audio data. \(error.userInfo)")
        }
        
        cell.timeElapsedLabel.text = ""
        cell.timeRemainingLabel.text = ""
        cell.progressionSlider.value = 0.0
        cell.progressionSlider.maximumValue = Float(avPlayer.duration)
        cell.avPlayer = avPlayer
        cell.indexPath = indexPath
        cell.indexOfDatabase = indexPath.row
        
        if (!self.isEditing){
            if (cell.indexPath.row == (selectedRowIndex as IndexPath).row) {
                cell.enableCellElements()
            } else {
                cell.disableCellElements()
            }
        } else {
            cell.setEditing(true, animated: true)
            if (selectedRecordsIndexes.contains(indexPath.row)){
                cell.setSelected(true, animated: false)
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! CustomTableViewCell
        if (!self.isEditing){
            //Normal mode
            selectedRowIndex = indexPath as NSIndexPath
            tableView.reloadRows(at: [indexPath], with: .fade)
            
            for cell: CustomTableViewCell in tableView.visibleCells as! [CustomTableViewCell] {
                cell.disableCellElements()
            }
            
            if indexPath.row == selectedRowIndex.row {
                (tableView.cellForRow(at: indexPath) as! CustomTableViewCell).enableCellElements()
            }
        } else {
            //Editing mode
            cell.setEditing(true, animated: true)
            if cell.isSelected {
                selectedRecordsIndexes.append(cell.indexOfDatabase!)
            } else {
                selectedRecordsIndexes.remove(at: (selectedRecordsIndexes.firstIndex(of: cell.indexOfDatabase!))!)
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if (!self.isEditing){
            self.tableView.endEditing(true)
            
            for cell: CustomTableViewCell in tableView.visibleCells as! [CustomTableViewCell] {
                if (cell.indexPath.row != (selectedRowIndex as IndexPath).row) {
                    cell.disableCellElements()
                }
            }
        } else {
            self.tableView.endEditing(true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (!self.isEditing){
            if indexPath.row == selectedRowIndex.row {
                return 201
            }
            return 70
        } else {
            return 70
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  (resultSearchController.isActive) {
            return filteredTableData.count
        } else {
            return records.count
        }
    }
    
    //MARK: To enable sliding actions
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if (records.count > 0){
            return true
        }
        return false
    }
    
    //MARK: - Swiping actions
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let more = UIContextualAction(style: .normal, title: "...") { (action, self, true)  in
            (tableView.cellForRow(at: indexPath) as! CustomTableViewCell).optionAction((tableView.cellForRow(at: indexPath) as! CustomTableViewCell).optionButton)
        }
        more.backgroundColor = .gray
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, banana, true) in
            let alert = UIAlertController(title: "Warning", message: "Are you sure you want to delete this recording?", preferredStyle: .alert)
            let confirmAction = UIAlertAction(title: "Yes", style: .destructive) { (action) in
                
                let commit = self.records[indexPath.row]
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
                let managedContext = appDelegate.persistentContainer.viewContext
                
                do{
                    managedContext.delete(commit)
                    tableView.beginUpdates()
                    self.records.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    tableView.endUpdates()
                    try managedContext.save()
                    tableView.reloadData()
                } catch let error as NSError{
                    print("CIAO, \(error.userInfo)")
                }
            }
            let cancelAction = UIAlertAction(title: "No", style: .cancel)
            alert.addAction(confirmAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true)
        }
        delete.backgroundColor = .red
        delete.image = UIImage(systemName: "trash.fill")
        let actions = UISwipeActionsConfiguration(actions: [delete, more])
        return actions
    }
    
    //MARK: Alternative way to delete a cell
    //    //MARK: - To delete a cell
    //    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    //        if editingStyle == .delete {
    //
    //            let alert = UIAlertController(title: "Warning", message: "Are you sure you want to delete this recording?", preferredStyle: .alert)
    //            let confirmAction = UIAlertAction(title: "Yes", style: .destructive) { (action) in
    //
    //                let commit = self.records[indexPath.row]
    //                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
    //                let managedContext = appDelegate.persistentContainer.viewContext
    //
    //                do{
    //                    managedContext.delete(commit)
    //                    tableView.beginUpdates()
    //                    self.records.remove(at: indexPath.row)
    //                    tableView.deleteRows(at: [indexPath], with: .fade)
    //                    tableView.endUpdates()
    //                    try managedContext.save()
    //                    tableView.reloadData()
    //                } catch let error as NSError{
    //                    print("CIAO, \(error.userInfo)")
    //                }
    //            }
    //            let cancelAction = UIAlertAction(title: "No", style: .cancel)
    //            alert.addAction(confirmAction)
    //            alert.addAction(cancelAction)
    //            present(alert, animated: true)
    //
    //        }
    //    }
    
    //MARK: - Extension for AV
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }
    
    //MARK: - Extension for search bar
    func updateSearchResults(for searchController: UISearchController) {
        filteredTableData.removeAll(keepingCapacity: false)
        
        let searchPredicate = NSPredicate(format: "SELF.title CONTAINS[c] %@", searchController.searchBar.text!)
        let array = (records as NSArray).filtered(using: searchPredicate)
        filteredTableData = array as! [NSManagedObject]
        
        self.tableView.reloadData()
    }
}

//MARK: - Extension Location Manager
extension ListViewController: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            guard let placemarks = placemarks else {return}
            let placemark = placemarks as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks[0]
                if (placemark.locality != nil) {
                    self.locality = placemark.locality
                }
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error)")
    }
}
